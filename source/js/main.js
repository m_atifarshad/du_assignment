/**
 * Created by Mohammad Atif on 19-Oct-16.
 */

(function(global, jQ){

    'use strict';


    //set defaults
    var iuFilter = (jQ('.filterOptions input[type=radio][name=internetUse]'));
    var minuteFilter = (jQ('.filterOptions input[type=radio][name=minuteUse]'));

    var default_isootope_options = {
        itemSelector:'.offer-item',
        layoutMode:'fitRows',
        filter:'.light'
    };

    //initialize isotope library
    jQ('.planOffers').isotope(default_isootope_options);

    //filter for internet use
    iuFilter.on('change',function(){

        var _self = $(this);
        var requested_filter = _self.val();
        var filter_selector = _self.data('filter');

        // remove previously selected clss
        _self
            .parents('.filterOptions')
            .find('label.selected')
            .removeClass('selected');

        //add selected class to the current selected element
        _self
            .parent()
            .addClass('selected');

        //generate notification based on user selection
        switch (requested_filter){
            case 'lightUse':
                generateNotification('information', 'You Selected <strong>Light</strong> Usage');
                break;
            case 'mediumUse':
                generateNotification('information', 'You Selected <strong>Medium</strong> Usage');
                break;
            case 'heavyUse':
                generateNotification('information', 'You Selected <strong>Heavy</strong> Usage');
                break;
            default:
                generateNotification('error', 'Some problem with your request, try again!');
                break;
        }

        jQ('.planOffers').isotope({
            filter:filter_selector
        });


    });

    minuteFilter.on('change',function(){
        var _self = $(this);
        var requested_filter = _self.val();

        // remove previously selected clss
        _self
            .parents('.filterOptions')
            .find('label.selected')
            .removeClass('selected');

        //add selected class to the current selected element
        _self
            .parent()
            .addClass('selected');

        //generate notification based on user selection
        switch (requested_filter){
            case 'localMinutes':
                generateNotification('success', 'You Selected <strong>Only Local</strong> call options');
                break;
            case 'internationalMinutes':
                generateNotification('success', 'You Selected <strong>International and Local</strong> call options');
                break;
            default:
                generateNotification('error', 'Some problem with your request, try again!');
                break;
        }

    });


    function generateNotification(type, text) {

        var n = noty({
            text: text,
            type: type,
            dismissQueue: true,
            progressBar: true,
            timeout: 5000,
            layout: 'topCenter',
            closeWith: ['click'],
            theme: 'relax',
            maxVisible: 10,
            animation: {
                open: 'animated fadeIn',
                close: 'animated fadeOut',
                easing: 'swing',
                speed: 500
            }
        });
        console.log('html: ' + n.options.id);
        return n;
    }


    //filters
})(window, jQuery);
